package org.bitbucket.socialroboticshub;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.bitbucket.socialroboticshub.DetectionResultProto.DetectionResult;

import com.google.cloud.dialogflow.v2.QueryResult;
import com.google.protobuf.Value;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public final class GoogleAssistant extends SICdevice {
	private static final String[] topics = { "dialogflow_agent", "assistant_show", "assistant_show_card",
			"assistant_play_media" };
	private static final long timeout = 5000L; // 5s
	private final WebHook webHook;
	private final WebSocket webSocket;
	private final ValueHolder response;

	public static void main(final String... args) {
		try {
			final String[] info = getConnectionInformation();
			final GoogleAssistant assistant = new GoogleAssistant(info[0], info[1], info[2]);
			assistant.run();
		} catch (final Exception e) {
			SICdevice.showError(e);
			System.exit(-1);
		}
	}

	private GoogleAssistant(final String server, final String user, final String pass) throws Exception {
		super(server, user, pass, "ga");
		final String[] hookInfo = getWebhookInformation();
		this.webHook = hookInfo[0].isEmpty() ? null : new WebHook(this, hookInfo[0]);
		this.webSocket = hookInfo[1].isEmpty() ? null : new WebSocket(this);
		if (this.webHook == null && this.webSocket == null) {
			throw new Exception("Please fill in either the loca.lt subdomain or select to use the VU server");
		}
		this.response = new ValueHolder();
	}

	private static String[] getWebhookInformation() throws Exception {
		final Preferences prefs = Preferences.userRoot().node("sic");
		final int fieldWidth = 16;

		final JDialog dialog = new JDialog((JDialog) null, "Dialogflow Webhook Information", true);
		final JPanel grid = new JPanel(new GridLayout(0, 1));
		final String domainText = "<html><b>[ONLY LOCAL]</b> Use localtunnel for a direct end-to-end webhook</html>";
		final JLabel domainInfo = new JLabel(domainText);
		grid.add(domainInfo);
		final JPanel domainPanel = new JPanel(new BorderLayout());
		final JLabel domainLabel = new JLabel("loca.lt subdomain: ");
		final JTextField domainField = new JTextField(fieldWidth);
		domainField.setText(prefs.get("domain", ""));
		domainPanel.add(domainLabel, BorderLayout.WEST);
		domainPanel.add(domainField, BorderLayout.EAST);
		grid.add(domainPanel);
		final String projectText = "<html><b>[SHARED MODE]</b> Use the common VU server (leave loca.lt blank)&nbsp;</html>";
		final JLabel projectInfo = new JLabel(projectText);
		grid.add(projectInfo);
		final JPanel projectPanel = new JPanel(new BorderLayout());
		final JCheckBox projectField = new JCheckBox("Use " + WebSocket.VUserver);
		projectField.setSelected(!prefs.get("project", "").isEmpty());
		projectPanel.add(projectField, BorderLayout.WEST);
		grid.add(projectPanel);

		final JButton ok = new JButton("OK");
		ok.addActionListener(e -> {
			dialog.dispose();
		});
		grid.add(ok);

		dialog.add(grid);
		dialog.getRootPane().setDefaultButton(ok);
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(final WindowEvent e) {
				if (domainField.getText().isEmpty()) {
					domainField.requestFocusInWindow();
				} else {
					projectField.requestFocusInWindow();
				}
			}
		});
		dialog.pack();
		dialog.setVisible(true);

		final String[] result = { domainField.getText().trim(), projectField.isSelected() ? "1" : "" };
		prefs.put("domain", result[0]);
		prefs.put("project", result[1]);
		return result;
	}

	@Override
	protected void disconnect() {
		super.disconnect();
		if (this.webHook != null) {
			this.webHook.stop();
		}
		if (this.webSocket != null) {
			this.webSocket.close();
		}
	}

	public String getResponse(final QueryResult queryResult) throws Exception {
		this.response.clear();
		if (processRecognisedIntent(queryResult)) {
			final Timer timer = new Timer(true);
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					System.out.println("Timed out whilst waiting for response...");
					GoogleAssistant.this.response.set("");
				}
			}, timeout); // cancel waiting when Google gives up
			final String response = this.response.get(); // blocking call
			timer.cancel();
			publishEvent("ShownOnAssistant");
			return response;
		} else {
			return "";
		}
	}

	private boolean processRecognisedIntent(final QueryResult intent) {
		final DetectionResult.Builder result = DetectionResult.newBuilder();
		result.setSource("webhook");

		final int confidence = (int) (intent.getIntentDetectionConfidence() * 100);
		if (confidence > 0) { // an intent was detected
			final String name = intent.getIntent().getDisplayName();
			System.out.format("DETECTED INTENT: '%s' (confidence %d%%)\n", name, confidence);
			result.setIntent(intent.getAction());
			final Map<String, Value> parameters = intent.getParameters().getFieldsMap();
			result.putAllParameters(parameters);
			if (!parameters.isEmpty()) {
				System.out.println(parameters);
			}
			result.setConfidence(confidence);
		}

		final String queryText = intent.getQueryText();
		if (!queryText.isEmpty()) { // the final recognised text (if any)
			System.out.format("RECOGNISED TEXT: '%s'\n", queryText);
			result.setText(queryText);
		}

		if (confidence > 0 || !queryText.isEmpty()) {
			publish("audio_intent", result.build().toByteArray());
			return true;
		} else {
			return false;
		}
	}

	public void run() throws Exception {
		try (final Jedis redis = connect()) {
			redis.ping();
			System.out.println("Subscribing '" + this.identifier + "' to " + this.server);
			redis.subscribe(new JedisPubSub() {
				@Override
				public void onMessage(final String rawchannel, final String message) {
					try {
						final String channel = getChannelWithoutId(rawchannel);
						if (channel.equals(topics[0])) {
							if (GoogleAssistant.this.webSocket != null) {
								GoogleAssistant.this.webSocket.start(message);
							}
						} else {
							System.out.println("GOT RESPONSE " + channel + ": " + message);
							GoogleAssistant.this.response.set(channel + ":" + message);
						}
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}
			}, getTopicsWithId(topics));
		}
	}
}
