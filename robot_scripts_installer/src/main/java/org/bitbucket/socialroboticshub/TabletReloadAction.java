package org.bitbucket.socialroboticshub;

import javax.swing.text.PlainDocument;

import com.jcraft.jsch.Session;

final class TabletReloadAction extends RobotAction {
	private final String serverIp;

	TabletReloadAction(final RobotMainFrame parent, final String[] robotInfo, final String serverIp) {
		super(parent, new PlainDocument(), robotInfo);
		this.serverIp = (serverIp.startsWith("127.") || serverIp.equals("localhost")) ? getLANAddress() : serverIp;
	}

	@Override
	public void run() {
		Session session = null;
		try {
			session = getSession();
			callCommand(session, "qicli call ALTabletService.resetTablet > /dev/null 2>&1");
			final String url = "https://" + this.serverIp + ":11880/index.html";
			callCommand(session, "qicli call ALTabletService.showWebview " + url + "> /dev/null 2>&1");
		} catch (final Exception e) {
			SICdevice.showError(e);
		} finally {
			if (session != null) {
				session.disconnect();
			}
		}
	}
}
