package org.bitbucket.socialroboticshub;

import java.awt.EventQueue;

import com.formdev.flatlaf.FlatLightLaf;

public final class RobotInstaller extends SICdevice {
	public static void main(final String... args) {
		try {
			final String[] info = getConnectionInformation();
			final RobotInstaller installer = new RobotInstaller(info[0], info[1], info[2]);
			installer.run();
		} catch (final Exception e) {
			SICdevice.showError(e);
			System.exit(-1);
		}
	}

	private RobotInstaller(final String server, final String user, final String pass) throws Exception {
		super(server, user, pass, null);
	}

	public void run() throws Exception {
		EventQueue.invokeLater(() -> {
			try {
				FlatLightLaf.setup();
				new RobotMainFrame(RobotInstaller.this).setVisible(true);
			} catch (final Exception e) {
				SICdevice.showError(e);
				System.exit(-1);
			}
		});
	}
}
