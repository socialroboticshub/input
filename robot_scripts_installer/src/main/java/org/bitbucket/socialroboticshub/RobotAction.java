package org.bitbucket.socialroboticshub;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.text.Document;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;

abstract class RobotAction extends Thread {
	protected final static String ROBOT_DIR = "/home/nao/sic";
	protected final static String EXEC_SH = ("stdbuf -i0 -oL -eL bash -lc " + ROBOT_DIR);
	protected final RobotMainFrame parent;
	protected final ShellStream output;
	protected final String robotIp, robotUser, robotPass, robotExtra, robotBranch;
	protected final boolean isPuppet, skipDownload;

	RobotAction(final RobotMainFrame parent, final Document output, final String[] robotInfo) {
		this.parent = parent;
		this.output = new ShellStream(output);
		this.robotIp = robotInfo[0];
		this.robotUser = robotInfo[1];
		this.robotPass = robotInfo[2];
		this.robotExtra = robotInfo[3].equals("1") ? "-p" : "";
		this.robotBranch = robotInfo[4];
		this.isPuppet = robotInfo[5].equals("1");
		this.skipDownload = robotInfo[6].equals("1");
	}

	protected Session getSession() throws Exception {
		final Session session = new JSch().getSession(this.robotUser, this.robotIp);
		session.setConfig("StrictHostKeyChecking", "no");
		session.setConfig("PreferredAuthentications", "keyboard-interactive");
		session.setConfig("MaxAuthTries", "1");
		session.setUserInfo(new RobotUserInfo(this.robotPass));
		session.connect();
		return session;
	}

	protected void callCommand(final Session session, final String command) throws Exception {
		ChannelExec channel = null;
		try {
			channel = (ChannelExec) session.openChannel("exec");
			channel.setInputStream(null);
			channel.setOutputStream(this.output);
			channel.setErrStream(this.output);
			channel.setCommand(command);
			channel.connect();
			while (!channel.isClosed()) {
				try {
					Thread.sleep(1000);
				} catch (final InterruptedException ie) {
					break;
				}
			}
		} finally {
			if (channel != null) {
				channel.disconnect();
			}
		}
	}

	private static final class RobotUserInfo implements UserInfo, UIKeyboardInteractive {
		private final String passwd;

		RobotUserInfo(final String passwd) {
			this.passwd = passwd;
		}

		@Override
		public String getPassword() {
			return null;
		}

		@Override
		public String getPassphrase() {
			return null;
		}

		@Override
		public boolean promptYesNo(final String message) {
			return false;
		}

		@Override
		public boolean promptPassphrase(final String message) {
			return false;
		}

		@Override
		public boolean promptPassword(final String message) {
			return false;
		}

		@Override
		public void showMessage(final String message) {
			System.out.println(message);
		}

		@Override
		public String[] promptKeyboardInteractive(final String destination, final String name, final String instruction,
				final String[] prompt, final boolean[] echo) {
			if (echo.length == 1 && !echo[0]) {
				return new String[] { this.passwd };
			} else {
				return null;
			}
		}
	}

	protected static String getLANAddress() {
		final Map<String, String> addresses = new LinkedHashMap<>();
		try {
			final Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
			while (e.hasMoreElements()) {
				final NetworkInterface network = e.nextElement();
				final byte[] mac = network.getHardwareAddress();
				if (Identifier.isValidMac(mac)) {
					final Enumeration<InetAddress> e2 = network.getInetAddresses();
					while (e2.hasMoreElements()) {
						final InetAddress address = e2.nextElement();
						if (address instanceof Inet4Address && !address.isLoopbackAddress()) {
							addresses.put(network.getDisplayName(), address.getHostAddress());
							break;
						}
					}
				}
			}
		} catch (final Exception ignore) {
		}

		if (addresses.isEmpty()) {
			return "";
		} else if (addresses.size() == 1) {
			return addresses.values().iterator().next();
		} else {
			final Object selected = JOptionPane.showInputDialog(null, null, "Select Network", JOptionPane.PLAIN_MESSAGE,
					null, addresses.keySet().toArray(new String[addresses.size()]), null);
			return addresses.get(selected);
		}
	}
}
