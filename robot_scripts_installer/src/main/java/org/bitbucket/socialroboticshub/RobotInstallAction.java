package org.bitbucket.socialroboticshub;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.text.Document;

import com.jcraft.jsch.Session;

final class RobotInstallAction extends RobotAction {
	private final static String BASE_URL = "https://bitbucket.org/socialroboticshub/docker";
	private final static String CACERT_URL = "https://curl.se/ca/cacert.pem";
	private final static String FILE_INDEX = "/sic/robot_scripts/.index";
	private final static String CERT_FILE = "/sic/robot_scripts/cacert.pem";
	private final String serverIp, serverUser, serverPass;
	private final boolean isLocal;

	RobotInstallAction(final RobotMainFrame parent, final String serverIp, final String serverUser,
			final String serverPass, final Document output, final boolean isLocal) {
		super(parent, output, getRobotInformation());
		this.serverIp = (serverIp.startsWith("127.") || serverIp.equals("localhost")) ? getLANAddress() : serverIp;
		this.serverUser = serverUser;
		this.serverPass = serverPass;
		this.isLocal = isLocal;
	}

	@Override
	public void run() {
		Session session = null;
		try {
			session = getSession();

			this.parent.setRobotInfo(this.robotIp, this.robotUser, this.robotPass, this.robotExtra, this.robotBranch,
					this.isPuppet ? "1" : "0", this.skipDownload ? "1" : "0");

			callCommand(session, EXEC_SH + "/stop.sh > /dev/null 2>&1");

			if (!this.skipDownload) {
				final String rm = "echo \"Setting up connection...\" && rm -rf " + ROBOT_DIR;
				final String indexUrl = BASE_URL + "/raw/" + this.robotBranch + FILE_INDEX;
				final String wget = "wget -q -i " + indexUrl + " -P " + ROBOT_DIR;
				callCommand(session, rm + " && " + wget + " && echo \"Download OK!\"");
				final String certUrl = this.isLocal ? (BASE_URL + "/raw/" + this.robotBranch + CERT_FILE) : CACERT_URL;
				callCommand(session, "wget -q --no-check-certificate " + certUrl + " -P " + ROBOT_DIR);
				final String subdir = (ROBOT_DIR + "/sic");
				final String move = "mkdir " + subdir + " && mv " + ROBOT_DIR + "/__init__.py " + subdir //
						+ " && mv " + ROBOT_DIR + "/device.py " + subdir;
				callCommand(session, move);
			}
			final String template = this.isPuppet ? "start_puppet.sh.template" : "start.sh.template";
			final String copy = "cp -f " + ROBOT_DIR + "/" + template + " " + ROBOT_DIR + "/start.sh";
			final String sed = "sed -i " + ("-e 's/unknown1/" + this.serverIp + "/' ")
					+ ("-e 's/unknown2/" + this.serverUser + "/' ") + ("-e 's/unknown3/" + this.serverPass + "/' ")
					+ ("-e 's/unknown4/" + this.robotExtra + "/' ") + (ROBOT_DIR + "/start.sh");
			final String chmod = "chmod +x " + ROBOT_DIR + "/*.sh";
			callCommand(session, copy + " && " + sed + " && " + chmod);

			callCommand(session, EXEC_SH + "/start.sh");
		} catch (final Exception e) {
			SICdevice.showError(e);
		} finally {
			if (session != null) {
				session.disconnect();
			}
		}
	}

	private static String[] getRobotInformation() {
		final Preferences prefs = Preferences.userRoot().node("sic");
		final int fieldWidth = 12;

		final JDialog dialog = new JDialog((JDialog) null, "Robot Info", true);
		final JPanel grid = new JPanel(new GridLayout(0, 1));
		final JPanel ipPanel = new JPanel(new BorderLayout());
		final JLabel ipLabel = new JLabel("Robot IP: ");
		final JTextField ipField = new JTextField(fieldWidth);
		ipField.setText(prefs.get("robot", ""));
		ipPanel.add(ipLabel, BorderLayout.WEST);
		ipPanel.add(ipField, BorderLayout.EAST);
		grid.add(ipPanel);
		final JPanel passwordPanel = new JPanel(new BorderLayout());
		final JLabel passwordLabel = new JLabel("Password: ");
		final JPasswordField passwordField = new JPasswordField(fieldWidth);
		passwordPanel.add(passwordLabel, BorderLayout.WEST);
		passwordPanel.add(passwordField, BorderLayout.EAST);
		grid.add(passwordPanel);
		final JPanel branchPanel = new JPanel(new BorderLayout());
		final JLabel branchLabel = new JLabel("Branch: ");
		final JTextField branchField = new JTextField(fieldWidth);
		branchField.setText(prefs.get("branch", "master"));
		branchPanel.add(branchLabel, BorderLayout.WEST);
		branchPanel.add(branchField, BorderLayout.EAST);
		grid.add(branchPanel);
		final JPanel profilingPanel = new JPanel(new BorderLayout());
		final JLabel profilingLabel = new JLabel("Profiling:      ");
		final JCheckBox profilingField = new JCheckBox();
		profilingPanel.add(profilingLabel, BorderLayout.WEST);
		profilingPanel.add(profilingField, BorderLayout.CENTER);
		grid.add(profilingPanel);
		final JPanel puppetPanel = new JPanel(new BorderLayout());
		final JLabel puppetLabel = new JLabel("Puppet:        ");
		final JCheckBox puppetField = new JCheckBox();
		puppetPanel.add(puppetLabel, BorderLayout.WEST);
		puppetPanel.add(puppetField, BorderLayout.CENTER);
		grid.add(puppetPanel);
		final JPanel skipDownloadPanel = new JPanel(new BorderLayout());
		final JLabel skipDownloadLabel = new JLabel("No internet: ");
		final JCheckBox skipDownloadField = new JCheckBox();
		skipDownloadPanel.add(skipDownloadLabel, BorderLayout.WEST);
		skipDownloadPanel.add(skipDownloadField, BorderLayout.CENTER);
		grid.add(skipDownloadPanel);

		final JButton ok = new JButton("OK");
		ok.addActionListener(e -> {
			dialog.dispose();
		});
		grid.add(ok);

		dialog.add(grid);
		dialog.getRootPane().setDefaultButton(ok);
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(final WindowEvent e) {
				if (ipField.getText().isEmpty()) {
					ipField.requestFocusInWindow();
				} else {
					passwordField.requestFocusInWindow();
				}
			}
		});
		dialog.pack();
		dialog.setResizable(false);
		dialog.setVisible(true);

		final String[] result = { ipField.getText().trim(), "nao", new String(passwordField.getPassword()),
				profilingField.isSelected() ? "1" : "0", branchField.getText(), puppetField.isSelected() ? "1" : "0",
				skipDownloadField.isSelected() ? "1" : "0" };
		if (!result[0].isEmpty()) {
			prefs.put("robot", result[0]);
		}
		if (!result[4].isEmpty()) {
			prefs.put("branch", result[4]);
		}
		return result;
	}
}
