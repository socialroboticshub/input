package org.bitbucket.socialroboticshub;

import javax.swing.text.Document;

import com.jcraft.jsch.Session;

final class RobotUninstallAction extends RobotAction {
	RobotUninstallAction(final RobotMainFrame parent, final Document output, final String[] robotInfo) {
		super(parent, output, robotInfo);
	}

	@Override
	public void run() {
		Session session = null;
		try {
			session = getSession();

			callCommand(session, EXEC_SH + "/stop.sh");

			this.parent.unsetRobotInfo();
		} catch (final Exception e) {
			SICdevice.showError(e);
		} finally {
			if (session != null) {
				session.disconnect();
			}
		}
	}
}
