package org.bitbucket.socialroboticshub;

import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;
import javax.swing.text.Document;

final class RobotMainFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final String GENERAL_SHELL = "general";
	private static final String[] SHELL_NAMES = { "general", "action_consumer", "audio_consumer", "audio_producer",
			"event_producer", "tablet", "video_producer" };
	private final SICdevice parent;
	private final int width, height;
	private final Map<String, JTextArea> shells = new HashMap<>();
	private JButton connect, disconnect, tabletRefresh, tabletSettings;
	private String[] robotInfo;

	RobotMainFrame(final SICdevice parent) {
		super("Robot Installer");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.parent = parent;

		final Rectangle screen = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
		this.width = (int) (screen.getWidth() / 2);
		this.height = (int) (screen.getHeight() / 2);
		setBounds(0, 0, this.width, this.height);
		setResizable(false);

		final JPanel main = new JPanel(null);
		final JPanel left = getLeftPanel();
		final int leftWidth = left.getComponent(0).getWidth();
		left.setBounds(0, 0, leftWidth, this.height);
		main.add(left);
		final JTabbedPane center = getCenterPanel();
		center.setBounds(leftWidth, 0, (int) (this.width - leftWidth * 1.2), (int) (this.height * 0.9));
		main.add(center);

		getContentPane().add(main);
	}

	public void setRobotInfo(final String robotIp, final String robotUser, final String robotPass,
			final String robotExtra, final String robotBranch, final String isPuppet, final String skipDownload) {
		this.robotInfo = new String[] { robotIp, robotUser, robotPass, robotExtra, robotBranch, isPuppet,
				skipDownload };
		this.connect.setEnabled(false);
		this.disconnect.setEnabled(true);
		this.tabletRefresh.setEnabled(true);
		this.tabletSettings.setEnabled(true);

		if (isPuppet.equals("0")) {
			for (final String shell : SHELL_NAMES) {
				if (!shell.equals(GENERAL_SHELL)) {
					final Document out = this.shells.get(shell).getDocument();
					final RobotAction log = new RobotLogAction(RobotMainFrame.this, shell, out, this.robotInfo);
					log.start();
				}
			}
		} else {
			final Document out = this.shells.get(GENERAL_SHELL).getDocument();
			final RobotAction log = new RobotLogAction(RobotMainFrame.this, "puppet", out, this.robotInfo);
			log.start();
		}
	}

	public void unsetRobotInfo() {
		this.robotInfo = null;
		this.connect.setEnabled(true);
		this.disconnect.setEnabled(false);
		this.tabletRefresh.setEnabled(false);
		this.tabletSettings.setEnabled(false);
	}

	private JPanel getLeftPanel() {
		final JPanel left = new JPanel(null);
		final JPanel buttonContainer = new JPanel();
		buttonContainer.setBounds(0, 0, this.width / 6, this.height);
		buttonContainer.add(getLeftCenter());
		left.add(buttonContainer);
		return left;
	}

	private JPanel getLeftCenter() {
		final JPanel panel = new JPanel(new GridLayout(0, 1, 0, 5));

		this.connect = new JButton("Connect");
		this.connect.addActionListener(e -> {
			try {
				final Document output = this.shells.get(GENERAL_SHELL).getDocument();
				final RobotAction install = new RobotInstallAction(RobotMainFrame.this, this.parent.server,
						this.parent.user, this.parent.password, output, this.parent.isLocal);
				install.start();
			} catch (final Exception ex) {
				SICdevice.showError(ex);
			}
		});
		panel.add(this.connect);

		this.disconnect = new JButton("Disconnect");
		this.disconnect.addActionListener(e -> {
			try {
				final Document output = this.shells.get(GENERAL_SHELL).getDocument();
				final RobotAction uninstall = new RobotUninstallAction(RobotMainFrame.this, output, this.robotInfo);
				uninstall.start();
			} catch (final Exception ex) {
				SICdevice.showError(ex);
			}
		});
		this.disconnect.setEnabled(false);
		panel.add(this.disconnect);

		panel.add(new JLabel()); // spacing
		final JLabel pingLabel = new JLabel();
		new Thread(() -> {
			do {
				try {
					final double ping = this.parent.ping();
					pingLabel.setText("ping: " + String.format("%.1f", ping) + "ms");
					Thread.sleep(5000);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			} while (isVisible());
		}).start();
		panel.add(pingLabel);
		panel.add(new JLabel()); // spacing

		this.tabletRefresh = new JButton("Tablet Reload");
		this.tabletRefresh.addActionListener(e -> {
			try {
				final RobotAction reload = new TabletReloadAction(RobotMainFrame.this, this.robotInfo,
						this.parent.server);
				reload.start();
			} catch (final Exception ex) {
				SICdevice.showError(ex);
			}
		});
		this.tabletRefresh.setEnabled(false);
		panel.add(this.tabletRefresh);

		this.tabletSettings = new JButton("Tablet Settings");
		this.tabletSettings.addActionListener(e -> {
			try {
				final RobotAction settings = new TabletSettingsAction(RobotMainFrame.this, this.robotInfo);
				settings.start();
			} catch (final Exception ex) {
				SICdevice.showError(ex);
			}
		});
		this.tabletSettings.setEnabled(false);
		panel.add(this.tabletSettings);

		return panel;
	}

	private JTabbedPane getCenterPanel() {
		final JTabbedPane tabs = new JTabbedPane();
		for (final String shellName : SHELL_NAMES) {
			final JTextArea shell = new JTextArea();
			shell.setLineWrap(true);
			shell.setWrapStyleWord(true);
			this.shells.put(shellName, shell);

			final JScrollPane scroller = new JScrollPane(shell, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scroller.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
				private int prev = 0;

				@Override
				public void adjustmentValueChanged(final AdjustmentEvent e) {
					final int max = e.getAdjustable().getMaximum();
					if (max != this.prev) { // autoscroll
						e.getAdjustable().setValue(max);
						this.prev = max;
					}
				}
			});
			tabs.add(shellName, scroller);
		}

		return tabs;
	}
}
