package org.bitbucket.socialroboticshub;

import javax.swing.text.PlainDocument;

import com.jcraft.jsch.Session;

final class TabletSettingsAction extends RobotAction {
	TabletSettingsAction(final RobotMainFrame parent, final String[] robotInfo) {
		super(parent, new PlainDocument(), robotInfo);
	}

	@Override
	public void run() {
		Session session = null;
		try {
			session = getSession();
			callCommand(session, "qicli call ALTabletService._openSettings > /dev/null 2>&1");
		} catch (final Exception e) {
			SICdevice.showError(e);
		} finally {
			if (session != null) {
				session.disconnect();
			}
		}
	}
}
