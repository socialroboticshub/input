package org.bitbucket.socialroboticshub;

import javax.swing.text.Document;

import com.jcraft.jsch.Session;

final class RobotLogAction extends RobotAction {
	private final String name;

	RobotLogAction(final RobotMainFrame parent, final String name, final Document output, final String[] robotInfo) {
		super(parent, output, robotInfo);
		this.name = (name + ".log");
	}

	@Override
	public void run() {
		Session session = null;
		try {
			session = getSession();
			final String tail = "tail -f " + ROBOT_DIR + "/" + this.name + " 2>/dev/null";
			callCommand(session, "while ! " + tail + " ; do sleep 1 ; done");
		} catch (final Exception ignore) {
		} finally {
			if (session != null) {
				session.disconnect();
			}
		}
	}
}
