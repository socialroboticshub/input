package org.bitbucket.socialroboticshub;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

final class ShellStream extends OutputStream {
	private final Document document;

	ShellStream(final Document document) {
		this.document = document;
	}

	@Override
	public void write(final int b) throws IOException {
		try {
			this.document.insertString(this.document.getLength(), Byte.toString((byte) b), null);
		} catch (final BadLocationException bl) {
			throw new IOException(bl);
		}
	}

	@Override
	public void write(final byte b[], final int off, final int len) throws IOException {
		try {
			final byte[] bytes = new byte[len];
			System.arraycopy(b, off, bytes, 0, len);
			this.document.insertString(this.document.getLength(), new String(bytes, StandardCharsets.UTF_8), null);
		} catch (final BadLocationException bl) {
			throw new IOException(bl);
		}
	}
}