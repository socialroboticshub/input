package org.bitbucket.socialroboticshub;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public final class SICmicrophone extends SICdevice implements LineListener {
	private final static int sampleRate = 16000; // sampleSize is always 16
	private final AudioFormat format;
	private final TargetDataLine dataline;
	private final JLabel status;
	private int index = -1;

	public static void main(final String... args) {
		try {
			final String[] info = getConnectionInformation();
			final SICmicrophone mic = new SICmicrophone(info[0], info[1], info[2]);
			mic.run();
		} catch (final Exception e) {
			SICdevice.showError(e);
			System.exit(-1);
		}
	}

	private SICmicrophone(final String server, final String user, final String pass) throws Exception {
		super(server, user, pass, "mic");
		this.format = new AudioFormat(sampleRate, 16, 1, true, false);

		final DataLine.Info info = new DataLine.Info(TargetDataLine.class, this.format);
		final Map<String, Mixer> mixers = new LinkedHashMap<>();
		for (final Mixer.Info mixerinfo : AudioSystem.getMixerInfo()) {
			final Mixer mixer = AudioSystem.getMixer(mixerinfo);
			if (mixer.isLineSupported(info) && !mixerinfo.getName().startsWith("Primary Sound")) {
				mixers.put(mixerinfo.getName(), mixer);
			}
		}
		final int found = mixers.size();
		Mixer mixer = null;
		switch (found) {
		case 0:
			throw new Exception("No suitable input device found!");
		case 1:
			mixer = mixers.values().iterator().next();
			break;
		default:
			final Object pick = JOptionPane.showInputDialog(null, "", "Input Device", JOptionPane.QUESTION_MESSAGE,
					null, mixers.keySet().toArray(new Object[found]), null);
			mixer = mixers.get(pick);
			break;
		}
		this.dataline = (TargetDataLine) mixer.getLine(info);
		this.dataline.addLineListener(this);

		final JFrame window = new JFrame("SIC Microphone");
		window.setLayout(new BorderLayout());
		final JButton identifier = new JButton(this.identifier);
		identifier.setEnabled(false);
		window.add(identifier, BorderLayout.NORTH);

		this.status = new JLabel();
		toggleStatus(false);
		window.add(this.status, BorderLayout.WEST);

		showWindow(window);
	}

	private void toggleStatus(final boolean listening) {
		if (listening) {
			publishEvent("ListeningStarted;1;" + sampleRate);
			this.status.setText("Listening!");
			this.status.setForeground(Color.GREEN);
		} else {
			publishEvent("ListeningDone");
			this.status.setText("Not listening...");
			this.status.setForeground(Color.RED);
		}
	}

	public void run() throws Exception {
		try (final Jedis redis = connect()) {
			System.out.println("Subscribing '" + this.identifier + "' to " + this.server);
			redis.subscribe(new JedisPubSub() {
				@Override
				public void onMessage(final String channel, final String message) {
					try {
						final double seconds = Double.parseDouble(message);
						if (seconds >= 0) {
							startListening(seconds);
						} else {
							stopListening();
						}
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}
			}, this.identifier + "_action_audio");
		}
	}

	private void startListening(final double seconds) throws Exception {
		if (this.dataline.isOpen()) {
			System.err.println("Already listening...");
		} else {
			this.index++;
			this.dataline.open(this.format);
			this.dataline.start();
			toggleStatus(true);
			if (seconds > 0) {
				final int myIndex = this.index;
				new Thread(() -> {
					try {
						Thread.sleep((long) (seconds * 1000));
						if (myIndex == SICmicrophone.this.index) {
							stopListening();
						}
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}).start();
			}
		}
	}

	private void stopListening() throws Exception {
		if (this.dataline.isOpen()) {
			this.dataline.stop();
			this.dataline.flush();
			this.dataline.close();
			toggleStatus(false);
		} else {
			System.err.println("Already not listening...");
		}
	}

	@Override
	public void update(final LineEvent event) {
		System.out.println(event.getType());
		if (event.getType() == LineEvent.Type.OPEN) {
			try {
				new RedisMicrophoneSync().start();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	private final class RedisMicrophoneSync extends Thread {
		private final Jedis redis;
		private final byte[] audiotopic;

		RedisMicrophoneSync() throws Exception {
			this.redis = connect();
			this.audiotopic = (SICmicrophone.this.identifier + "_audio_stream").getBytes(UTF8);
		}

		@Override
		public void run() {
			final byte[] next = new byte[SICmicrophone.this.dataline.getBufferSize()];
			while (SICmicrophone.this.dataline.isOpen()) {
				final int read = SICmicrophone.this.dataline.read(next, 0, next.length);
				if (read > 0) {
					this.redis.rpush(this.audiotopic, next);
				}
			}
			this.redis.close();
		}
	}
}
