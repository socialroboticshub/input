package org.bitbucket.socialroboticshub;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public final class SICconsole extends SICdevice {
	public static void main(final String... args) {
		try {
			final String[] info = getConnectionInformation();
			final SICconsole console = new SICconsole(info[0], info[1], info[2]);
			console.run();
		} catch (final Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private SICconsole(final String server, final String user, final String pass) throws Exception {
		super(server, user, pass, null);
	}

	public void run() throws Exception {
		System.out.println("Connected as '" + this.identifier + "', enter your commands...");
		System.out.print("> ");
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			String line;
			while ((line = reader.readLine()) != null) {
				try {
					System.out.println(query(line));
					System.out.print("> ");
				} catch (final Exception e) {
					System.err.println(e.getMessage());
					System.out.println("> ");
				}
			}
		}
	}
}
