package org.bitbucket.socialroboticshub;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

final class Identifier {
	private final static String macFormat = "%02X";
	private final static byte invalidMacs[][] = { // https://stackoverflow.com/a/32170974
			{ 0x00, 0x05, 0x69 }, // VMWare
			{ 0x00, 0x1C, 0x14 }, // VMWare
			{ 0x00, 0x0C, 0x29 }, // VMWare
			{ 0x00, 0x50, 0x56 }, // VMWare
			{ 0x08, 0x00, 0x27 }, // Virtualbox
			{ 0x0A, 0x00, 0x27 }, // Virtualbox
			{ 0x00, 0x03, (byte) 0xFF }, // Virtual-PC
			{ 0x00, 0x15, 0x5D } // Hyper-V
	};

	private Identifier() { // static class
	}

	public static String getIdentifier() throws Exception {
		final Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
		while (networks.hasMoreElements()) {
			final NetworkInterface network = networks.nextElement();
			final byte[] mac = network.getHardwareAddress();
			if (isValidMac(mac)) {
				final Enumeration<InetAddress> addresses = network.getInetAddresses();
				while (addresses.hasMoreElements()) {
					final InetAddress address = addresses.nextElement();
					if (address instanceof Inet4Address && !address.isLoopbackAddress()) {
						final StringBuilder sb = new StringBuilder();
						for (final byte el : mac) {
							sb.append(String.format(macFormat, el));
						}
						return sb.toString();
					}
				}
			}
		}
		throw new Exception("Could not find a suitable network interface");
	}

	public static boolean isValidMac(final byte[] mac) {
		if (mac == null || mac.length == 0) {
			return false;
		} else {
			for (final byte[] invalid : invalidMacs) {
				if (mac[0] == invalid[0] && mac[1] == invalid[1] && mac[2] == invalid[2]) {
					return false;
				}
			}
			return true;
		}
	}
}