package org.bitbucket.socialroboticshub;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.Preferences;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Protocol;
import redis.clients.jedis.Protocol.Command;
import redis.clients.jedis.util.SafeEncoder;

public abstract class SICdevice {
	protected static final Charset UTF8 = StandardCharsets.UTF_8;
	protected final String server;
	protected final boolean isLocal;
	protected final String user;
	protected final String password;
	protected final String type;
	protected final String device;
	protected final String identifier;
	private final Jedis publisher;
	private volatile boolean isRunning;

	public SICdevice(final String server, final String user, final String password, final String type)
			throws Exception {
		this(server, user, password, type, Identifier.getIdentifier());
	}

	public SICdevice(final String server, final String user, final String password, final String type,
			final String device) throws Exception {
		this.server = server;
		this.isLocal = isLocal(server);
		this.user = user;
		this.password = password;
		this.type = type;
		this.device = device;
		this.identifier = user + "-" + this.device;
		this.publisher = connect();
		if (type != null) {
			announceAsDevice();
		}
	}

	private static boolean isLocal(final String input) {
		return (input.startsWith("127.") || input.startsWith("192.") || input.equals("localhost"));
	}

	protected static String[] getConnectionInformation() throws Exception {
		final Preferences prefs = Preferences.userRoot().node("sic");
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		final int fieldWidth = 12;

		final JDialog dialog = new JDialog((JDialog) null, "SIC Info", true);
		final JPanel grid = new JPanel(new GridLayout(0, 1));
		final JPanel serverPanel = new JPanel(new BorderLayout());
		final JLabel serverLabel = new JLabel("Server: ");
		final JTextField serverField = new JTextField(fieldWidth);
		serverField.setText(prefs.get("server", "127.0.0.1"));
		serverPanel.add(serverLabel, BorderLayout.WEST);
		serverPanel.add(serverField, BorderLayout.EAST);
		grid.add(serverPanel);
		final JPanel userPanel = new JPanel(new BorderLayout());
		final JLabel userLabel = new JLabel("Username: ");
		final JTextField userField = new JTextField(fieldWidth);
		userField.setText(prefs.get("user", ""));
		userPanel.add(userLabel, BorderLayout.WEST);
		userPanel.add(userField, BorderLayout.EAST);
		grid.add(userPanel);
		final JPanel passwordPanel = new JPanel(new BorderLayout());
		final JLabel passwordLabel = new JLabel("Password: ");
		final JPasswordField passwordField = new JPasswordField(fieldWidth);
		passwordPanel.add(passwordLabel, BorderLayout.WEST);
		passwordPanel.add(passwordField, BorderLayout.EAST);
		grid.add(passwordPanel);

		final DocumentListener isLocal = new DocumentListener() {
			@Override
			public void insertUpdate(final DocumentEvent e) {
				changedUpdate(e);
			}

			@Override
			public void removeUpdate(final DocumentEvent e) {
				changedUpdate(e);
			}

			@Override
			public void changedUpdate(final DocumentEvent e) {
				if (isLocal(serverField.getText())) {
					userField.setEnabled(false);
					passwordField.setEnabled(false);
				} else {
					userField.setEnabled(true);
					passwordField.setEnabled(true);
				}
			}
		};
		serverField.getDocument().addDocumentListener(isLocal);
		isLocal.changedUpdate(null);

		final JButton ok = new JButton("OK");
		ok.addActionListener(e -> {
			dialog.dispose();
		});
		grid.add(ok);

		dialog.add(grid);
		dialog.getRootPane().setDefaultButton(ok);
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(final WindowEvent e) {
				if (userField.isEnabled() && userField.getText().isEmpty()) {
					userField.requestFocusInWindow();
				} else if (passwordField.isEnabled()) {
					passwordField.requestFocusInWindow();
				} else {
					serverField.requestFocusInWindow();
				}
			}
		});
		dialog.pack();
		dialog.setResizable(false);
		dialog.setVisible(true);

		final String[] result = { serverField.getText().trim().replace("localhost", "127.0.0.1"),
				userField.isEnabled() ? userField.getText().trim() : "default",
				passwordField.isEnabled() ? new String(passwordField.getPassword()) : "changemeplease" };
		if (!result[0].isEmpty()) {
			prefs.put("server", result[0]);
		}
		if (userField.isEnabled() && !result[1].isEmpty()) {
			prefs.put("user", result[1]);
		}
		return result;
	}

	private void announceAsDevice() throws Exception {
		final Jedis jedis = connect();
		new Thread(() -> {
			this.isRunning = true;
			final String user = "user:" + this.user;
			final String device = this.device + ":" + this.type;
			while (this.isRunning) {
				try {
					final List<String> time = jedis.time();
					jedis.zadd(user, Integer.parseInt(time.get(0)), device);
					Thread.sleep(60 * 1000 - 100);
				} catch (final Exception e) {
					e.printStackTrace();
					break;
				}
			}
			try {
				jedis.zrem(user, device);
			} catch (final Exception ignore) {
			}
			jedis.close();
		}).start();
	}

	protected void disconnect() {
		this.isRunning = false;
		this.publisher.close();
	}

	protected String[] getTopicsWithId(final String[] topics) {
		final String[] topicsWithId = new String[topics.length];
		for (int i = 0; i < topics.length; i++) {
			topicsWithId[i] = this.identifier + "_" + topics[i];
		}
		return topicsWithId;
	}

	protected byte[][] getRawTopicsWithId(final String[] topics) {
		final byte[][] topicsWithId = new byte[topics.length][];
		for (int i = 0; i < topics.length; i++) {
			topicsWithId[i] = (this.identifier + "_" + topics[i]).getBytes(UTF8);
		}
		return topicsWithId;
	}

	protected String getChannelWithoutId(final String channel) {
		return channel.substring(this.identifier.length() + 1);
	}

	protected void showWindow(final JFrame frame) {
		frame.setPreferredSize(new Dimension(400, 100));
		frame.pack();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	protected synchronized double ping() {
		final long start = System.nanoTime();
		this.publisher.ping();
		return (System.nanoTime() - start) / 1000000.0;
	}

	protected synchronized void publish(final String channel, final String data) {
		this.publisher.publish(this.identifier + "_" + channel, data);
	}

	protected synchronized void publish(final String channel, final byte[] data) {
		this.publisher.publish((this.identifier + "_" + channel).getBytes(UTF8), data);
	}

	protected synchronized void stream(final String channel, final byte[] data) {
		this.publisher.rpush((this.identifier + "_" + channel).getBytes(UTF8), data);
	}

	protected synchronized void publishEvent(final String name) {
		this.publisher.publish(this.identifier + "_events", name);
	}

	protected synchronized String query(final String query) {
		final String[] q = query.split(" ");
		final Command cmd = Command.valueOf(q[0].toUpperCase());
		final String[] args = Arrays.copyOfRange(q, 1, q.length);
		final Object result = this.publisher.sendCommand(cmd, args);
		return (result == null) ? "" : parseRedisResult(result);
	}

	private static String parseRedisResult(final Object rawresult) {
		String result = "";
		if (rawresult instanceof List<?>) {
			for (final Object subresult : (List<?>) rawresult) {
				if (!result.isEmpty()) {
					result += "\n";
				}
				result += parseRedisResult(subresult);
			}
		} else if (rawresult instanceof byte[]) {
			result += SafeEncoder.encode((byte[]) rawresult);
		} else if (rawresult instanceof Long) {
			result += Long.toString((Long) rawresult);
		} else {
			result += "Unknown result of type: " + rawresult.getClass();
		}
		return result;
	}

	protected Jedis connect() throws Exception {
		final Jedis jedis = (this.isLocal || "1".equals(System.getenv("DB_SSL_SELFSIGNED")))
				? new Jedis(this.server, Protocol.DEFAULT_PORT, true, getLocalSSL(), null, null)
				: new Jedis(this.server, Protocol.DEFAULT_PORT, true);
		jedis.auth(this.user, this.password);
		return jedis;
	}

	protected SSLSocketFactory getLocalSSL() throws Exception {
		final KeyStore keyStore = KeyStore.getInstance("JKS");
		keyStore.load(SICdevice.class.getResourceAsStream("/truststore.jks"), "changeit".toCharArray());
		final Certificate original = ((KeyStore.TrustedCertificateEntry) keyStore.getEntry("sic", null))
				.getTrustedCertificate();
		final TrustManager bypass = new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[] { (X509Certificate) original };
			}

			@Override
			public void checkClientTrusted(final X509Certificate[] chain, final String authType)
					throws CertificateException {
				checkServerTrusted(chain, authType);
			}

			@Override
			public void checkServerTrusted(final X509Certificate[] chain, final String authType)
					throws CertificateException {
				if (chain.length != 1 || !chain[0].equals(original)) {
					throw new CertificateException("Invalid certificate provided");
				}
			}
		};
		final SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(null, new TrustManager[] { bypass }, null);
		return sslContext.getSocketFactory();
	}

	public static void showError(final Exception e) {
		e.printStackTrace();
		JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	}
}
