package org.bitbucket.socialroboticshub;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.openpnp.capture.CaptureDevice;
import org.openpnp.capture.CaptureStream;
import org.openpnp.capture.OpenPnpCapture;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.Pipeline;

public final class SICcamera extends SICdevice {
	private final CaptureDevice webcam;
	private final ImagePanel display;
	private final JFrame popup;
	private final JLabel status;
	private boolean openWindow;
	private volatile boolean isWatching;
	private int index = -1;

	public static void main(final String... args) {
		try {
			final String[] info = getConnectionInformation();
			final SICcamera video = new SICcamera(info[0], info[1], info[2]);
			video.run();
		} catch (final Exception e) {
			SICdevice.showError(e);
			System.exit(-1);
		}
	}

	private SICcamera(final String server, final String user, final String pass) throws Exception {
		super(server, user, pass, "cam");

		final OpenPnpCapture capture = new OpenPnpCapture();
		final List<CaptureDevice> devices = capture.getDevices();
		final int found = devices.size();
		switch (found) {
		case 0:
			throw new Exception("No suitable input device found!");
		case 1:
			this.webcam = devices.get(0);
			break;
		default:
			final Object pick = JOptionPane.showInputDialog(null, "", "Input Device", JOptionPane.QUESTION_MESSAGE,
					null, devices.toArray(new Object[found]), null);
			this.webcam = (CaptureDevice) pick;
			break;
		}

		this.display = new ImagePanel();
		this.display.setPreferredSize(new Dimension(640, 480));
		this.popup = new JFrame();
		this.popup.add(this.display);
		this.popup.pack();

		final JFrame window = new JFrame("SIC Camera");
		window.setLayout(new BorderLayout());
		final JButton identifier = new JButton(this.identifier);
		identifier.setEnabled(false);
		window.add(identifier, BorderLayout.NORTH);

		this.status = new JLabel();
		window.add(this.status, BorderLayout.WEST);

		final JCheckBox toggleOpenWindow = new JCheckBox("Show feed", this.openWindow);
		toggleOpenWindow.addActionListener(e -> SICcamera.this.openWindow = toggleOpenWindow.isSelected());
		window.add(toggleOpenWindow, BorderLayout.EAST);

		showWindow(window);
	}

	public void run() throws Exception {
		try (final Jedis redis = connect()) {
			System.out.println("Subscribing '" + this.identifier + "' to " + this.server);
			redis.subscribe(new JedisPubSub() {
				@Override
				public void onMessage(final String channel, final String message) {
					try {
						final String[] split = message.split(";");
						final double seconds = Double.parseDouble(split[0]);
						if (seconds >= 0) {
							startWatching(seconds);
						} else {
							stopWatching();
						}
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}
			}, this.identifier + "_action_video");
		}
	}

	private void startWatching(final double seconds) throws Exception {
		if (this.isWatching) {
			System.err.println("Already watching...");
		} else {
			this.index++;
			new RedisWebcamSync().start();
			this.isWatching = true;
			if (seconds > 0) {
				final int myIndex = this.index;
				new Thread(() -> {
					try {
						Thread.sleep((long) (seconds * 1000));
						if (myIndex == SICcamera.this.index) {
							stopWatching();
						}
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}).start();
			}
		}
	}

	private void stopWatching() {
		if (this.isWatching) {
			this.isWatching = false;
		} else {
			System.err.println("Already not watching...");
		}
	}

	private final class RedisWebcamSync extends Thread {
		private final Jedis redis;
		private final byte[] videotopic;
		private final String sizetopic, frametopic;
		private int width, height;

		RedisWebcamSync() throws Exception {
			this.redis = connect();
			this.videotopic = (SICcamera.this.identifier + "_image_stream").getBytes(UTF8);
			this.sizetopic = SICcamera.this.identifier + "_image_size";
			this.frametopic = SICcamera.this.identifier + "_image_available";
		}

		@Override
		public void run() {
			final CaptureStream stream = SICcamera.this.webcam.openStream(SICcamera.this.webcam.getFormats().get(0));
			while (SICcamera.this.isWatching) {
				try {
					if (!stream.hasNewFrame()) {
						Thread.sleep(1);
						continue;
					}

					final BufferedImage img = stream.capture();
					final long unixTime = System.currentTimeMillis();

					final Pipeline pipe = this.redis.pipelined();
					if (this.width == 0 && this.height == 0) {
						this.width = img.getWidth();
						this.height = img.getHeight();
						pipe.set(this.sizetopic, this.width + " " + this.height + " RGB");
						pipe.publish(SICcamera.this.identifier + "_events", "WatchingStarted");
						SICcamera.this.status.setText("Watching!");
						SICcamera.this.status.setForeground(Color.GREEN);
						if (SICcamera.this.openWindow) {
							SICcamera.this.popup.setVisible(true);
						}
					}
					if (SICcamera.this.openWindow) {
						SICcamera.this.display.setImage(img);
					}

					final byte[] holder = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
					final byte[] data = new byte[holder.length];
					for (int i = 0; i < holder.length; i += 3) { // BGR to RGB
						data[i] = holder[i + 2];
						data[i + 1] = holder[i + 1];
						data[i + 2] = holder[i];
					}

					pipe.zadd(this.videotopic, unixTime, data);
					pipe.zremrangeByRank(this.videotopic, 0, -5);
					pipe.publish(this.frametopic, Long.toString(unixTime));
					pipe.sync();
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
			stream.close();
			this.redis.publish(SICcamera.this.identifier + "_events", "WatchingDone");
			SICcamera.this.status.setText("Not watching...");
			SICcamera.this.status.setForeground(Color.RED);
			this.redis.close();
			if (SICcamera.this.openWindow) {
				SICcamera.this.popup.setVisible(false);
			}
		}
	}

	private static class ImagePanel extends JPanel {
		private static final long serialVersionUID = 1L;
		private BufferedImage img = null;

		public void setImage(final BufferedImage img) {
			this.img = img;
			SwingUtilities.invokeLater(this::repaint);
		}

		@Override
		public void paintComponent(final Graphics g) {
			super.paintComponent(g);
			g.drawImage(this.img, 0, 0, getWidth(), getHeight(), this);
		}
	}
}
